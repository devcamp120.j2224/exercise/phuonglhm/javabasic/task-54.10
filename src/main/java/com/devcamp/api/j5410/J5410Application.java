package com.devcamp.api.j5410;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5410Application {

	public static void main(String[] args) {
		SpringApplication.run(J5410Application.class, args);
	}

}
